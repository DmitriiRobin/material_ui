import React from 'react'
import Button from '@material-ui/core/Button'
import { makeStyles, createStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => createStyles({
    root: {
        background: theme.palette.error.main,
        color: '#fff',
        '&:hover': {
            opacity: .7,
            background: theme.palette.error.main,
        },
    },
  }))

function ErrorButton(props) {
    const classes = useStyles()

    return (
        <Button {...props} className={props.error ? classes.root : ''}>
            {props.error ? 'Error_Button' : props.children}
        </Button>
    )
}

export default ErrorButton
