import React from 'react'
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import UserButton from './ErrorButton'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'

function App() {
  const theme = React.useMemo(
    () =>
      createMuiTheme({
        spacing: 4,
        palette: {
          primary: {
            main: '#0F4780',
          },
          secondary: {
            main: '#6B64FF',
          },
          error: {
            main: '#e33371',
          },
        },
        overrides: {
          MuiButton: {
            root: {
              width: '100%',
              padding: '10px 16px',
              '&.MuiButton-containedSecondary:hover': {
                opacity: 0,
              }
            },
          },
          MuiPaper: {
            root: {
              width: '400px',
              padding: '20px',
              '&.MuiPaper-rounded': {
                borderRadius: '8px',
              }
            },
          },
          breakpoints: {
            values: {
              md: '700px',
            },
          },
        },
      }),
    [],
  );

  return (
    <ThemeProvider theme={theme}>
      <Grid alignItems="center" container direction="column">
        <Button 
          variant="contained"
          color="secondary"
        >
          Button_Contained
        </Button>
        <Paper variant="elevation" elevation={3} square={false} item="true">
              <UserButton color="primary" variant="contained">User_Button1</UserButton>
              <UserButton color="primary" error="true" variant="contained">User_Button2</UserButton>
              <UserButton color="primary" variant="contained">User_Button3</UserButton>
              <UserButton color="primary" variant="contained">User_Button4</UserButton>
        </Paper>
      </Grid>
    </ThemeProvider>
  )
}

export default App
